# Files in POSIX and beyond

Files are at the heard of the POSIX abstraction. We don't just use 'em to store data, as you would expect. They are *magical*. You can use them to do a lot of good, but with great power comes great responsibility, especially given that in some cases you can [hard brick your system by deleting the right (or wrong, depending on your point of view) files](https://github.com/systemd/systemd/issues/2402).

In this post I will summarize what I have found out about files. I'll show you the APIs for each feature, and some examples on the console. There's a lot more to know about files than I thought before writing this post.

## POSIX standard file API

I went ahead and searched the current [POSIX specification](http://pubs.opengroup.org/onlinepubs/9699919799/) for all file-related functions. Some functions exist as duplicates where one version operates on a *file descriptor* and the other on a path, so I had to deduplicate them a little bit.

There are only a few headers in which you'll find the POSIX file-related functions, which are these:

```c
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
```

```c
// functions I haven't accounted for yet.
mode_t   umask(mode_t);
int      access(const char *, int);
int      fcntl(int, int, ...);
int      futimens(int, const struct timespec [2]);
int      posix_fadvise(int, off_t, off_t, int);
int      posix_fallocate(int, off_t, off_t);
int      lockf(int, int, off_t);
```

```c
int      lstat(const char *restrict, struct stat *restrict);
int      stat(const char *restrict, struct stat *restrict);
```

If you are familiar with C, you'll know that these methods are *not* the C standard IO methods. The C methods operate on `FILE` pointers, whereas functions of the POSIX API either take (or return) a file descriptor, which is an `int`, or a path, as a `NULL`-terminated, UTF-8 encoded `const char *`.

If we want to know what types of entries POSIX has, a good way of learning about them is by looking at the `stat` struct defined in [`sys/stat.h`](http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/sys_stat.h.html). All of these properties can be accessed with the `stat()` and `lstat()` functions — just in case you want to see for yourself.

    dev_t     st_dev     Device ID of device containing file. 
    ino_t     st_ino     File serial number. 
    mode_t    st_mode    Mode of file (see below). 
    nlink_t   st_nlink   Number of hard links to the file. 
    uid_t     st_uid     User ID of file. 
    gid_t     st_gid     Group ID of file. 
    dev_t     st_rdev    Device ID (if file is character or block special). 
    off_t     st_size    For regular files, the file size in bytes. 
                         For symbolic links, the length in bytes of the 
                         pathname contained in the symbolic link. 
                         For a shared memory object, the length in bytes. 
                         For a typed memory object, the length in bytes. 
                         For other file types, the use of this field is 
                         unspecified. 
    time_t    st_atime   Time of last access. 
    time_t    st_mtime   Time of last data modification. 
    time_t    st_ctime   Time of last status change. 
    blksize_t st_blksize A file system-specific preferred I/O block size for 
                         this object. In some file system types, this may 
                         vary from file to file. 
    blkcnt_t  st_blocks  Number of blocks allocated for this object. 

### Device ID and inode

The device ID (`st_dev`) of an entry is set by the operating system and tells you on which hardware the file resides (how this is mapped varies between implementations). The inode number (`st_ino`) is set by the file system and is basically a pointer to the file. This means that the combination of device ID and inode number uniquely identifies every file. That doesn't mean that you can't have multiple files with the same `{device_id, inode}` pair: see [hard links](#hard-links) for more on that.

### Mode

To know what types of entries there are in a POSIX-compliant system, we can consult the `st_mode` member, which encodes what type an entry has.

> The `<sys/stat.h>` header shall define the following symbolic constants for the file types encoded in type mode_t. The values shall be suitable for use in #if preprocessing directives:
> 
> -   `S_IFBLK`: Block special.
> -   `S_IFCHR`: Character special.
> -   `S_IFIFO`: FIFO special.
> -   `S_IFREG`: Regular.
> -   `S_IFDIR`: Directory.
> -   `S_IFLNK`: Symbolic link.
> -   `S_IFSOCK`: Socket.

I think anyone who has used computers before is familiar with at least two of these types: *directories* and *regular files*, these are exactly what you would expect. But what about the others?

I just went ahead and created one of each kind of entry that POSIX specifies. Some are straightforward and the commands should be familiar to anyone who has worked on a UNIX-like system before, but some types (block and character special) need root privileges to create and for the socket entry I really couldn't find simple tool to create one, so I just used a Ruby one-liner.

    $ touch entry-types/regular-file
    $ mkdir entry-types/directory
    $ ln -s regular-file entry-types/symbolic-link
    $ mkfifo entry-types/fifo-special
    $ sudo mknod entry-types/block-special b 0 0
    $ sudo mknod entry-types/char-special c 0 0
    $ ruby -r socket -e "Socket.unix_server_socket('entry-types/unix-socket')"

With this set up, we can investigate this mix of familiar and strange files, folders, devices, queues and links:

    $ ls -la entry-types
    total 16
    drwxr-xr-x  10 pelsen  staff       340 Feb 23 14:08 .
    drwxr-xr-x  11 pelsen  staff       374 Feb 23 14:07 ..
    -rw-r--r--   1 pelsen  staff         2 Feb 23 13:34 .gitignore
    brw-r--r--   1 root    staff    0,   0 Feb 23 14:08 block-special
    crw-r--r--   1 root    staff    0,   0 Feb 23 14:08 char-special
    drwxr-xr-x   2 pelsen  staff        68 Feb 23 14:08 directory
    prw-r--r--   1 pelsen  staff         0 Feb 23 14:08 fifo-special
    -rw-r--r--   1 pelsen  staff         0 Feb 23 14:08 regular-file
    lrwxr-xr-x   1 pelsen  staff        12 Feb 23 14:08 symbolic-link -> regular-file
    srwxr-xr-x   1 pelsen  staff         0 Feb 23 14:08 unix-socket

I have explicitly used the `-la` flags for the `ls` command, which tells it to use the long format and output all files. You can observe that `ls` has a character assigned to each type of file:

-   `-` for regular files
-   `b` for block special
-   `c` for character special
-   `d` for directories
-   `p` for fifo special
-   `l` for symbolic links
-   `s` for UNIX sockets

There's actually more types than these — I'll get to those later.

So what are these different files and what do they do?

#### Directories

```c
int      mkdir(const char *, mode_t);
int      rmdir(const char *);
DIR     *opendir(const char *dirname);
struct dirent *readdir(DIR *dirp);
int      closedir(DIR *dirp);
```

A directory is a container for entries. A list of it's contents can be acessed with the POSIX functions `opendir()`, `readdir()` and `closedir()`. They can be created with `mkdir()` and deleted with `rmdir()`.

#### Regular files

```c
int      creat(const char *, mode_t);
int      open(const char *, int, ...);
off_t    lseek(int, off_t, int);
ssize_t  read(int, void *, size_t);
ssize_t  write(int, const void *, size_t);
int      fsync(int);
int      close(int);
int      truncate(const char *, off_t);
```

A regular file can hold some data (it's contents), which can be accessed with `open()`, `read()`, `write()`, and `close()`. They can be created with `creat()` an deleted with `unlink()`.

##### Hard links

```c
int      link(const char *, const char *);
```

A hardlink is basically an alias for a file. Unlike symlinks, which can point to anything (even nonexistand things), these can only point to exising (regular) files.

###### File Makefile, lines 55–60:

```makefile
hard-link-example: entry-types/hard-link
	ls -la entry-types/hard-link entry-types/regular-file
	echo data > entry-types/hard-link
	ls -la entry-types/hard-link entry-types/regular-file
	cat entry-types/regular-file
```

    $ ls -la entry-types/hard-link entry-types/regular-file
    -rw-r--r--  2 pelsen  staff  5 Feb 25 19:41 entry-types/hard-link
    -rw-r--r--  2 pelsen  staff  5 Feb 25 19:41 entry-types/regular-file
    $ echo data > entry-types/hard-link
    $ ls -la entry-types/hard-link entry-types/regular-file
    -rw-r--r--  2 pelsen  staff  5 Feb 25 19:41 entry-types/hard-link
    -rw-r--r--  2 pelsen  staff  5 Feb 25 19:41 entry-types/regular-file
    $ cat entry-types/regular-file
    data

A file can have any number of aliases (hard links) pointing to it, and it will only really be deleted when all hard links have been `unlink`ed.

#### Symlink

```c
int      symlink(const char *, const char *);
ssize_t  readlink(const char *restrict, char *restrict, size_t);
int      unlink(const char *);
```

Symbolic link to another entry. Unlinke hard links, a symbolic link is not limited to pointing to regular files, it can point to something on another filesystem, and it can point to something using relative or absolute paths (each with their own caveats).

Symlinks are create with `symlink()`, removed with `unlink()` and the path they are pointing at can be read with `readlink()`.

###### File Makefile, lines 60–65:

```makefile
symlink-example: entry-types/symbolic-link
	ls -la entry-types/symbolic-link entry-types/regular-file
	echo data > entry-types/symbolic-link
	ls -la entry-types/symbolic-link entry-types/regular-file
	cat entry-types/regular-file
```

    $ ls -la entry-types/symbolic-link entry-types/regular-file
    -rw-r--r--  2 pelsen  staff   0 Feb 25 19:45 entry-types/regular-file
    lrwxr-xr-x  1 pelsen  staff  12 Feb 25 19:45 entry-types/symbolic-link -> regular-file
    $ echo data > entry-types/symbolic-link
    $ ls -la entry-types/symbolic-link entry-types/regular-file
    -rw-r--r--  2 pelsen  staff   5 Feb 25 19:45 entry-types/regular-file
    lrwxr-xr-x  1 pelsen  staff  12 Feb 25 19:45 entry-types/symbolic-link -> regular-file
    $ cat entry-types/regular-file
    data

Symbolic links can point to anything, they can be relative or absolute, and their target doesn't need to exist. Their (file) size is the length (in bytes) of the target string.

#### FIFO (named) pipes

```c
int      mkfifo(const char *, mode_t);
int      unlink(const char *);
```

*FIFO* means *first in, first out*. A FIFO special file is indeed special — if you write to it, no data is written to the filesystem. Instead, a write will block until another process reads from it, and the data is transferred directly between the processes.

It's relatively easy to illustrate how a FIFO operates. Using the FIFO that we created with the `mkfifo` tool, here's an example:

    $ cat entry-types/fifo-special &
    $ echo "a message sent to the FIFO queue." > entry-types/fifo-special
    a message sent to the FIFO queue.

#### UNIX Socket

Similar to *FIFO* pipes, this can be used for inter-process communication with two differences: it is a bidirectional connection, meaning that you can send data both ways—FIFO pipes always have one reading and one writing end. Also, it uses the socket API `send()` and `recv()` instead of the file API `write()` and `read()`.

Ruby has a relatively easy API to create and use UNIX sockets, using that it's simple to demonstrate how they work:

    $ ruby -r socket -e "Socket.unix_server_socket('entry-types/unix-socket'){|fd| puts fd.accept[0].read}" &
    $ ruby -r socket -e "Socket.unix('entry-types/unix-socket'){|fd| fd.puts('A message from a UNIX socket.')}"
    A message from a UNIX socket.

#### Block special

```c
int      mknod(const char *, mode_t, dev_t);
int      unlink(const char *);
```

Block special files are files that store a *device number*, which tells the kernel which *block device* the file represents. When you read or write to them, the kernel reads and writes to the actual device.

You may have seen (and possibly used) these before — hard drives are usually mapped into `/dev` as block special devices. On my MacOS system, I have these block devices in there:

    $ ls -la /dev | grep "^b"
    brw-r-----   1 root    operator         1,   0 Feb 19 14:59 disk0
    brw-r-----   1 root    operator         1,   1 Feb 19 14:59 disk0s1
    brw-r-----   1 root    operator         1,   3 Feb 19 14:59 disk0s2
    brw-r-----   1 root    operator         1,   2 Feb 19 14:59 disk0s3
    brw-r-----   1 root    operator         1,   4 Feb 19 14:59 disk1

On a Linux system you'll probably find a similar output, just that the devices are named differently.

The device number in this `ls` output is shown as two distinct numbers here: the *minor* and the *major*. These tell the kernel which block device the file should be associated with and the semantics of these numbers depends on the kernel.

I unfortunately couldn't find any information on the semantics of these for MacOS, I'm guessing that they are dynamically allocated. But for Linux, they are [well-documented](https://github.com/torvalds/linux/blob/master/Documentation/admin-guide/devices.txt). For example, if you had a listing somewhat like this:

    $ ls -la /dev | grep "^b"
    brw-rw----  1 root disk      8,   0 Feb 21 18:44 sda
    brw-rw----  1 root disk      8,   1 Feb 21 18:44 sda1
    brw-rw----  1 root cdrom    11,   0 Feb 21 18:44 sr0

You could, from that output, figure out that these three block devices represent the following hardware:

| File        | Major | Semantics             | Minor | Semantics |
| ----------- | ----- | --------------------- | ----- | --------- |
| `/dev/sda`  | 8     | *SCSI block device*   | 0     | *First SCSI disk, whole disk* |
| `/dev/sda1` | 8     | *SCSI block device*   | 1     | *First SCSI disk, first partition* |
| `/dev/sr0`  | 11    | *SCSI CD-ROM devices* | 0     | *First SCSI CD-ROM* |

An interesting fact: In the past, you had to actually create these block special files under `/dev` — nowadays, [udev](https://www.kernel.org/pub/linux/utils/kernel/hotplug/udev/udev.html) handles creating these, and the filesystem in `/dev` is a virtual one. You can confirm this using the `mount` utility with no arguments. On Linux, this is true:

    $ mount | grep " /dev "
    udev on /dev type devtmpfs (rw,nosuid,relatime,size=989720k,nr_inodes=247430,mode=755)

On my MacOS machine, I have a similar result:

    $ mount | grep " /dev "
    devfs on /dev (devfs, local, nobrowse)

Note that on both MacOS and Linux machines, `root` privileges are necessary to create these special files.

#### Character special

```c
int      mknod(const char *, mode_t, dev_t);
int      unlink(const char *);
```

In a similar vein to block special files, character special files store a *device number*, but they represent *character devices* which are devices to which one character can be written at a time.

You'll may have used them before if you have ever used some of the `magic` special files. Here's a little taste from my MacOS machine:

###### File Makefile, lines 77–79:

```makefile
ls-char-dev:
	ls -la /dev/null /dev/zero /dev/tty /dev/urandom
```

    $ ls -la /dev/null /dev/zero /dev/tty /dev/urandom
    crw-rw-rw-  1 root  wheel    3,   2 Feb 25 13:51 /dev/null
    crw-rw-rw-  1 root  wheel    2,   0 Feb 25 12:54 /dev/tty
    crw-rw-rw-  1 root  wheel   11,   1 Feb 19 14:59 /dev/urandom
    crw-rw-rw-  1 root  wheel    3,   3 Feb 19 14:59 /dev/zero

Same as with block special files, their content is never stored on the filesystem — read and writes are handled by the kernel, and depend only on the *device number*. Famously, reads from `/dev/zero` return all-zeroed memory, reads from `/dev/urandom` return random data, which is in fact the [preferred source of cryptographic randomness](https://www.2uo.de/myths-about-urandom/).

### Permissions

```c
int      chmod(const char *, mode_t);
int      chown(const char *, uid_t, gid_t);
```

Telling us the type that an entry has is not the only thing the `st_mode` member does: it also lets us know which permissions a file has. Permissions are commonly expressed as an octal number, because it's easier to read what is meant. You can break down permissions into four distinct categories which each have three flags that can be set.

| Other                   | Owner: `S_IRWXU` | Group: `S_IRWXG` | World: `S_IRWXO` |
| ----------------------- | ---------------- | ---------------- | ---------------- |
| Set user ID: `S_ISUID`  | Read: `S_IRUSR`  | Read: `S_IRGRP`  | Read: `S_IROTH`  |
| Set group ID: `S_ISGID` | Write: `S_IWUSR` | Write: `S_IWGRP` | Write: `S_IWOTH` |
| Sticky bit: `S_ISVTX`   | Exec: `S_IXUSR`  | Exec: `S_IXGRP`  | Exec: `S_IXOTH`  |

What do these permissions mean?

#### Read

If it's a file, you can `open()` it in read-only mode `O_RONLY` and `read()` from it. If it's a directory, it means you can list it's contents with `opendir()` and `readdir()`.

#### Write

If it's a file, it means you can `open()` it in write mode `O_WRONLY` and `write()` data to it. If it's a directory, this means you can create new entries inside with any of the file or directory creation routines.

#### Exec

If it's a file, this tells the operating system that you can run it as a binary (usually in ELF format on Linux, and Mach-O format on MacOS) or as a script (if it has a shebang as first line indicating the binary that it should be run with).

If it's a directory, this means that you are able to `chdir()` to it.

#### SUID and SGID

These only apply to binary files. When binary file is executed and is has the `SUID` bit set, it will be executed with it's `UID` set to that of the *owner*, meaning that is has all of the permissions of the owner, instead of as the user who is running it. If it has the `SGID` bit set, it will be executed with it's `GID` set to that of the group that owns it, instead of the group that the user that is running it belongs to.

As an example, `su` and `sudo` are commonly used `SUID` binaries:

    $ ls -la /usr/bin/{su,sudo}
    -rwsr-xr-x  1 root  wheel  25312  May  5  2016 /usr/bin/su
    -r-s--x--x  1 root  wheel  168448 May  5  2016 /usr/bin/sudo

When you run any of them, since they are both owned by `root` and have the `SUID` bit set, they will run with full `root` permissions.

#### Sticky bit

Historically used to force executables to [stick around in RAM](http://netbsd.gw.com/cgi-bin/man-cgi?sticky++NetBSD-current), it has had a number of uses but currently it's main use is for temp directories:

> A file in a sticky directory may only be removed or renamed by a user if the
> user has write permission for the directory and the user is the owner of the
> file, the owner of the directory, or the super-user.  This feature is
> usefully applied to directories such as /tmp which must be publicly writable
> but should deny users the license to arbitrarily delete or rename each
> others' files.

### Owners

The members `st_uid` (user ID) and `st_gid` and (group ID) encode who owns the file. ID here is a number, where usually 0 is the superuser (commonly called `root`) and all other users have a positive integer as ID.



### Times

There are three timestaps defined for each file:

-   `st_atime` (time of last access)
-   `st_mtime` (time of last data modification)
-   `st_ctime` (time of last status change)

Often times, the `st_atime` is disabled for performance reasons.

### Device IDs and inode numbers

Did you think that a file was uniquely identified with it's path in the file system? Well, that's not exactly correct. Every file is uniquely identified by exactly two numbers — the *device ID*, called `st_dev` in the stat struct, and the *inode number*, called `st_ino` in the stat struct.

Device IDs are determined by the kernel, and inode numbers are determined by the file system. Let's have a look at what these values look like. Here's what I get on my macOS machine:

    $ touch regular-file
    $ stat -f "Name: %N%n%tDevice: %d%n%tinode: %i%n" regular-file
    Name: regular-file
            Device: 16777220
            inode: 40881748

And, for comparison, here's what you could expect on a Linux machine (Ubuntu in this case). Note that the `stat` command line tool takes different arguments on Linux — ugh.

    $ touch regular-file
    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file
    $ Name: regular-file
            Device: 2049
            inode: 259297

So, what exactly does that tell us? A few things, actually. From the Linux kernel [documentation for devices](https://www.mjmwired.net/kernel/Documentation/devices.txt), we can piece together what the device ID means.

| Device ID | Major | Semantics           | Minor | Semantics |
| --------- | ----- | ------------------- | ----- | --------- |
| 2049      | 8     | *SCSI block device* | 1     | *First SCSI drive, first partition* |

A device ID in Linux is a 16-bit integer made up of two parts — the *major* and the *minor*, which are each 8-bit integers. The `major` specifies the driver that is used for the device, and the `minor` acts like a selector, specifying which part of the device we are using. From that and the table in the documentation we can tell that the file `regular-file` probably lies on `/dev/sda1`, which is the default name for the first parition on the first SCSI drive.

We can confirm that guess easily:

    $ df -h regular-file
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/sda1        19G  1.7G   17G  10% /

But what does the device ID on macOS stand for? Unfortunately, I couldn't find any info on that.

So, what happens when we delete the file and look at the device ID and the inode number again? Here's what happens on macOS:

    $ stat -f "Name: %N%n%tDevice: %d%n%tinode: %i%n" regular-file
    Name: regular-file
            Device: 16777220
            inode: 40881748
    $ rm regular-file
    $ touch regular-file
    $ stat -f "Name: %N%n%tDevice: %d%n%tinode: %i%n" regular-file
    Name: regular-file
            Device: 16777220
            inode: 40882021

And here's what happens on Linux:

    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file
    Name: regular-file
            Device: 2049
            inode: 259297
    $ rm -f regular-file
    $ touch regular-file
    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file
    Name: regular-file
            Device: 2049
            inode: 259297

Now that's interesting. On macOS, the inode changes, by at least a few hundred, whereas on Linux it stayed the same — at least in this test. I was only able to get Linux to yield a different inode number by deleting `regular-file`, creating a different one, and then re-creating `regular-file`.

    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file
    Name: regular-file
            Device: 2049
            inode: 259297
    $ rm -f regular-file
    $ touch other-file
    $ touch regular-file
    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file
    Name: regular-file
            Device: 2049
            inode: 259309

What do we learn from this?

-   device IDs uniquely identify filesystems
-   inode numbers uniquely identify files, but aren't stable when deleting files.

#### Hard links

So why do device IDs and inode numbers even exist, why don't we just use paths as unique identifiers? Because paths aren't unique — you can have multiple paths pointing to the exact same file, which is called a *hard link*. You will notice this because the hard link will have exactly the same device ID and inode number as the original file. Here's an example from macOS:

###### File Makefile, lines 9–10:

```makefile
LN    = ln
```

###### File Makefile, lines 87–90:

```makefile
# show device ID and inode number for a regular file and a hard link.
stat-device-inode-hard-link: FILES += hard-link
stat-device-inode-hard-link: hard-link stat-device-inode
```

    $ touch regular-file
    $ ln regular-file hard-link
    $ stat -f "Name: %N%n%tDevice: %d%n%tinode: %i%n" regular-file hard-link
    Name: regular-file
            Device: 16777220
            inode: 40882404
    Name: hard-link
            Device: 16777220
            inode: 40882404

And here we have the same example, but on the Linux machine:

    $ ln regular-file hard-link
    $ stat --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n" regular-file hard-link
    Name: regular-file
            Device: 2049
            inode: 259309
    Name: hard-link
            Device: 2049
            inode: 259309

As you can see, the device IDs and inode numbers are exactly the same. This means that a path is just an alias for a tuple of `{device_id, inode}` — basically like a pointer. Any number of paths can point to the same file.

Can we find out how many paths point to a given file? Sure thing! Again, on macOS:

    $ rm hard-link
    $ stat -f "Name: %N%n%tLink count: %l" regular-file
    Name: regular-file
            Link count: 1 
    $ ln regular-file hard-link
    $ stat -f "Name: %N%n%tLink count: %l" regular-file
    Name: regular-file
            Link count: 2

And the same on Linux:

    $ rm -f hard-link
    $ stat --printf="Name: %n\n\tLinks: %h\n" regular-file
    Name: regular-file
            Links: 1
    $ ln regular-file hard-link
    $ stat --printf="Name: %n\n\tLinks: %h\n" regular-file
    Name: regular-file
            Links: 2

Behind the scenes, the `st_nlink` member of the stat struct keeps track of how many (hard) links exist to a file. Why do we care? A file is only really deleted when there aren't any links to it. By checking how many hard links exist to a file, we can check someone is secretly keeping a reference to this file!

## inode flags

## Access Control Lists

As you have seen in the permissions bit about the

## Extended Attributes

```c
#include <sys/types.h>
#include <sys/xattr.h>
```

```c
ssize_t listxattr(const char *path, char *list, size_t size);
ssize_t llistxattr(const char *path, char *list, size_t size);
ssize_t flistxattr(int fd, char *list, size_t size);
```

## Forks

On some systems, the kernel and filesystem allow forks, also called *resource forks*, aside from the normal contents of a file.

MacOS, NTFS,

## Non-standard APIs

Let's investigate what APIs POSIX has for us, what APIs Linux has (xattr), what macOS has (streams), what they do and what they tell us about the filesystem.

macOS and BSD systems have whiteout nodes, which are special files that indicate to the kernel that a file is deleted, which is useful on union filesystems.

On MacOS, the `stat` struct also has a `st_birth` member which encodes when the file was created (birthed).

## Conclusion

I thought this was going to be a quick little post, turns out there's a lot more to files, folders and all the other strange entities than I thought. But that's okay, we're all here to learn, and I love learning about obscure features, especially when they make some sort of sense.

All of the examples from this post can be cloned with

    $ git clone https://gitlab.com/xfbs-blog/files-posix-beyond.git

Feel free to send in corrections or suggestions.
