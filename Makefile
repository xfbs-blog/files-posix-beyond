ENTRIES += entry-types/block-special
ENTRIES += entry-types/char-special
ENTRIES += entry-types/fifo-special
ENTRIES += entry-types/regular-file
ENTRIES += entry-types/directory
ENTRIES += entry-types/symbolic-link
ENTRIES += entry-types/unix-socket
FILES = regular-file
STAT  = stat
LN    = ln

ifeq ($(shell uname -s),Linux)
# this works for anything that uses GNU stat.
STAT_DEVICE_INODE = $(STAT) --printf="Name: %n\n\tDevice: %d\n\tinode: %i\n"
STAT_LINKS = $(STAT) --printf="Name: %n\n\tLinks: %h\n"
else
# this works on macOS' stat, which comes from BSD.
STAT_DEVICE_INODE = $(STAT) -f "Name: %N%n%tDevice: %d%n%tinode: %i%n"
STAT_LINKS = $(STAT) -f "Name: %N%n%tLinks: %l%n"
endif

# creates a block special file.
entry-types/block-special:
	sudo mknod entry-types/block-special b 0 0

# creates a character special file.
entry-types/char-special:
	sudo mknod entry-types/char-special c 0 0

# creates a fifo queue special file.
entry-types/fifo-special:
	mkfifo entry-types/fifo-special

# creates a regular file.
entry-types/regular-file:
	touch entry-types/regular-file

# creates a directory.
entry-types/directory:
	mkdir entry-types/directory

# creates a symbolic link (to regular-file).
entry-types/symbolic-link:
	ln -s regular-file entry-types/symbolic-link

# creates a UNIX socket file.
entry-types/unix-socket:
	ruby -r socket -e "Socket.unix_server_socket('entry-types/unix-socket')"

# creates a hard link to a regular file.
entry-types/hard-link: entry-types/regular-file
	ln entry-types/regular-file entry-types/hard-link

ls-entries: $(ENTRIES)
	ls -la entry-types
hard-link-example: entry-types/hard-link
	ls -la entry-types/hard-link entry-types/regular-file
	echo data > entry-types/hard-link
	ls -la entry-types/hard-link entry-types/regular-file
	cat entry-types/regular-file
symlink-example: entry-types/symbolic-link
	ls -la entry-types/symbolic-link entry-types/regular-file
	echo data > entry-types/symbolic-link
	ls -la entry-types/symbolic-link entry-types/regular-file
	cat entry-types/regular-file
fifo-example: entry-types/fifo-special
	cat entry-types/fifo-special &
	echo "a message sent to the FIFO queue." > entry-types/fifo-special
socket-example:
	ruby -r socket -e "Socket.unix_server_socket('entry-types/unix-socket'){|fd| puts fd.accept[0].read}" &
	ruby -r socket -e "Socket.unix('entry-types/unix-socket'){|fd| fd.puts('A message from a UNIX socket.')}"

ls-block-dev:
	ls -la /dev | grep "^b"

show-dev-fs:
	mount | grep " /dev "
ls-char-dev:
	ls -la /dev/null /dev/zero /dev/tty /dev/urandom

# default targets
default:

# show device ID and inode number for regular file.
stat-device-inode: $(FILES)
	$(STAT_DEVICE_INODE) $(FILES)

# show device ID and inode number for a regular file and a hard link.
stat-device-inode-hard-link: FILES += hard-link
stat-device-inode-hard-link: hard-link stat-device-inode

# show how many links there are to each file
stat-links: $(FILES)

	$(STAT_LINKS) $(FILES)

# show which drive a file is on
which-drive: $(FILES)
	df -h $(FILES)

# creates a hard link.
hard-link: regular-file
	$(LN) $< $@

# delete example files.
clean:
	$(RM) -r entry-types/*
clean: rm-hard-link

rm-hard-link:
	$(RM) hard-link
