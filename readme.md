# Files in POSIX and beyond

This repository contains code discussed in my [blog post](https://blog.xfbs.net/files-in-posix-and-beyond). Check out the [raw writeup](files-posix-beyond.md), or try some of the examples yourself.

## Trying the examples

Everything you need is in the `Makefile`. Note that the examples are only tested to work on macOS or Linux (Ubuntu).

Here's all the things you can try, in the order from the article:

    $ make stat-device-inode
    $ make which-drive
    $ make stat-device-inode-hard-link
    $ make stat-links
